FROM ruby:3.1.0-alpine

# Install Dependencies
RUN apk add --no-cache postgresql-client postgresql-dev libpq-dev nodejs build-base tzdata yarn git vips vips-dev

# Workdir
WORKDIR /myapp

# Install Gems
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle config set --local without 'test development'
RUN bundle install 

# Add a script to be executed every time the container starts.
COPY . .

# Run assets:precompile
RUN bundle exec rails assets:precompile

# Add a script to be executed every time the container starts.
EXPOSE 3000

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]

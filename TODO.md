# Missing Design Information

- No example for user navigation dropdown.
- No example for multiple image inputs during production creation.
- Unclear entrypoint.

# Design Implementation

- In Home Page, doesn't look the same as the design.
  - Banner is not fully implemented
- In New Product Page, doesn't look the same as the design.
  - Navbar doesn't change.
  - No back button change.
- In Update Profile Page, doesn't look the same as the design.
  - Navbar doesn't change.
  - No back button change.

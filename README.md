# Secondhand

Secondhand is marketplace for secondhand stuff (we usually called it as "Loak").
This application is written in Ruby on Rails version 7+.

# Development

## Prerequisite

This application requires the following software to work:
- Ruby v3.1.0
- Rails v7.4.0
- Postgresql v14.5
- Redis v7.0.7
- libvips v8.6+ or ImageMagick for image analysis and transformations


## Development Environment

1. Open project root directory.
1. Make sure postgres database user with current user name on system.
   exists .
1. Run `rails db:create db:schema:load db:seed`.

## Run Application

```
rails s
```

# Running in Production

To run this application in production, the following needs to be run before starting the server:

```sh
RAILS_ENV=production rails assets:precompile               # Compile assets 
RAILS_ENV=production RAILS_SERVE_STATIC_FILES=true rails s # Start server
```

# Contributing

**TODO**

## Architectural Pattern

This application use MVC based on Rails. See: https://guides.rubyonrails.org/getting_started.html#mvc-and-you

There are some rules on this project that ones **MUST** comply:
- Every data retrieval happens in Controller, with Notifications data as
  exception.
- Every logic that will be used on View **MUST** be written inside helper,
  it can be general helper or specific helper.
- Every query helper **MUST** be written inside Model.
- If a controller response to multiple format, please just use Rails way.
- Use `resources` when defining routes if possible.

# License

The application is available as open source under the terms of the [MIT License](./LICENSE).

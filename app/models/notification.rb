# frozen_string_literal: true

class Notification < ApplicationRecord
  belongs_to :user

  has_one_attached :image

  scope :unread, -> { where(read: false) }
end

# frozen_string_literal: true

class Product < ApplicationRecord
  belongs_to :category
  belongs_to :user
  has_many   :offers, dependent: :destroy
  has_many_attached :images, dependent: :destroy

  validates :name, presence: true
  validates :price, presence: true, numericality: { positive: true }
  validates :description, presence: true

  enum status: {
    draft: 'DRAFT',
    published: 'PUBLISHED',
  }

  def price_in_format
    "Rp #{price.to_i.to_fs(:delimited, delimiter: '.')}"
  end

  def notify_user
    notification = Notification.new(subject: 'Berhasil di terbitkan', body: "#{name}<br>#{price_in_format}",
                                    click_action: Rails.application.routes.url_helpers.product_path(id), user:)

    unless notification.save
      self.errors = notification.errors
      return false
    end

    notification.image.attach(images.first.blob) if images.attached?

    true
  end
end

class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  # GET /categories.json
  def index
    @categories = Category.all
  end

  # GET /categories/:id.json
  def show
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end
end

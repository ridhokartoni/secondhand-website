# frozen_string_literal: true

class NotificationsController < ApplicationController
  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token
  before_action :set_notification, only: %i[read]

  layout "full"

  # GET /notifications
  def index
    @notifications = current_user.notifications.order(created_at: :desc).paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.json
      format.html
    end
  end

  # PUT /notifications/:id/read
  def read
    @notification.read = true
    return render json: @notification.errors, status: :unprocessable_entity unless @notification.save
  end

  private

  def set_notification
    @notification = current_user.notifications.find(params[:id])
  end
end

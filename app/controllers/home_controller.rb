# frozen_string_literal: true

class HomeController < ApplicationController
  layout 'full'

  def index
    @products = Product.published.includes(:category).with_attached_images.where(sold: false).order(created_at: :desc).paginate(page: params[:page],  per_page: 10)
    @products = @products.all if index_params[:category_id].blank?
    @products = @products.where(category_id: index_params[:category_id]) if index_params[:category_id].present?
    @products = @products.where("name ILIKE ?", "%#{index_params[:q]}%")
    @categories = Category.all
  end

  private

  def index_params
    params.permit(:category_id, :q)
  end
end

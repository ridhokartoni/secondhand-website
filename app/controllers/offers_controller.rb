# frozen_string_literal: true

class OffersController < ApplicationController
  before_action :authenticate_user!
  before_action :block_if_profile_blank!, only: [:create]
  before_action :ensure_to_params, only: [:create]
  before_action :set_product, only: [:create]
  before_action :set_to, only: [:create]
  before_action :set_offer, only: [:update]
  before_action :only_seller, only: [:update]

  layout 'full'

  # GET /users/:user_id/offers
  def index
    @from = User.with_attached_avatar.find(params[:user_id])
    @offers = Offer.where(from_id: @from.id, to_id: current_user.id).where.not(from_id: current_user.id).order(created_at: :desc).paginate(page: params[:page], per_page: 10)
    @title = "Info Penawar"
  end

  # POST /offers
  def create
    @offer = Offer.new(offer_params.merge(from_id: current_user.id))

    perform_transaction do
      respond_to do |format|
        if @offer.save
          @offer.product.update(liked: true)
          @offer.notify_user

          format.html { redirect_to product_url(@offer.product_id), notice: 'Harga tawarmu berhasil dikirim ke penjual' }
          format.json { render :show, status: :created, location: @offer }
        else
          format.html { render :show, status: :unprocessable_entity }
          format.json { render json: @offer.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PUT /offers/:id
  def update
    perform_transaction do
      respond_to do |format|
        if @offer.update(update_offer_params)
          @offer.product.update(sold: true) if @offer.finished?

          format.html { redirect_to user_offers_path(@offer.from_id), notice: 'Status produk berhasil diperbarui' }
          format.json { render :show, location: @offer }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @offer.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private

  # TODO: Add better response
  def ensure_to_params
    render nothing: true, status: :not_implemented if offer_params[:to_id].eql?(current_user.id)
  end

  def only_seller
    render nothing: true, status: :not_implemented unless current_user.id.eql?(@offer.to_id)
  end

  def offer_params
    params.require(:offer).permit(:to_id, :product_id, :price)
  end

  def update_offer_params
    params.require(:offer).permit(:status)
  end

  def set_offer
    @offer = Offer.related_to(current_user).find(params[:id])
  end

  def set_product
    @product = Product.published.find(offer_params[:product_id])
  end

  def set_to
    @to = User.find(offer_params[:to_id])
  end
end

# frozen_string_literal: true

class ProfilesController < ApplicationController
  before_action :authenticate_user!

  layout 'full'

  # GET /profiles
  def show
    @title = "Lengkapi Info Akun"
  end

  # PUT /profiles
  def update
    current_user.update(user_params)

    respond_to do |format|
      if current_user.update(user_params)
        current_user.avatar.attach(user_params[:avatar]) if user_params[:avatar].present?

        format.html { redirect_to redirect_url, notice: 'Profil berhasil diperbarui!' }
        format.json { render :show, status: :ok }
      else
        format.html do 
          render :show, status: :unprocessable_entity
        end
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def redirect_url
    params[:redirect_url].present? ? params[:redirect_url] : root_path
  end

  def user_params
    params.require(:user).permit(:name, :city_id, :avatar, :address, :phone_number)
  end
end

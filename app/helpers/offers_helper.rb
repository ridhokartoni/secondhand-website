# frozen_string_literal: true

module OffersHelper
  def to_status_text(offer)
    return 'Penawaran produk' if offer.initiated?
    return 'Penawaran produk diterima' if offer.accepted?
    return 'Penawaran produk ditolak' if offer.denied?
    return 'Berhasil terjual' if offer.finished?

    'Penjualan dibatalkan'
  end

  def option_button_required?(offer)
    offer.initiated?
  end

  def status_button_required?(offer)
    offer.accepted?
  end
end

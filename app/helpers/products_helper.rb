# frozen_string_literal: true

module ProductsHelper
  def mine?
    return false if current_user.blank?

    @product.user_id.eql?(current_user.id)
  end

  def offered?
    return false if current_user.blank?

    @product.offers.where(from_id: current_user.id, status: :initiated).exists?
  end

  def purchased?
    return false if current_user.blank?

    @product.offers.where(from_id: current_user.id, status: :finished).exists?
  end

  def published?
    @product.published?
  end

  def all_products?
    params[:product].nil?
  end

  def liked_product?
    params[:product].present? && params[:product][:liked].eql?('true') && params[:product][:sold].eql?('false')
  end

  def sold_product?
    params[:product].present? && params[:product][:sold].eql?('true')
  end

  def all_product_class(classes)
    classes += ' active' if all_products?
    classes
  end

  def liked_product_class(classes)
    classes += ' active' if liked_product?
    classes
  end

  def sold_product_class(classes)
    classes += ' active' if sold_product?
    classes
  end
end

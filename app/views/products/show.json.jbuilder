# frozen_string_literal: true

json.product @product, partial: 'products/product', as: :product

if offered?
  json.offer @offer, partial: 'offers/offer', as: :offer
end

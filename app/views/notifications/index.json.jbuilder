
# frozen_string_literal: true

json.partial! 'application/pagination', resources: @notifications
json.notifications @notifications, partial: 'notification', as: :notification

# frozen_string_literal: true

json.meta do
  json.page_next resources.next_page
  json.page_previous resources.previous_page
  json.page_size resources.per_page
  json.page_count resources.total_pages
  json.total resources.total_entries
end

# frozen_string_literal: true

json.user current_user, partial: 'users/user', as: :user

json.id user.id
json.name user.name
json.email user.email
json.phone_number user.phone_number
json.address user.address

if user.city.present?
  json.city do
    json.id user.city_id
    json.name user.city.name
  end
end

if user.avatar.attached?
  json.avatar do
    json.filename user.avatar.filename
    json.url url_for(user.avatar)
  end
end

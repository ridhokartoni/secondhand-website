# frozen_string_literal: true

class AddColumnsOnProducts < ActiveRecord::Migration[7.0]
  def change
    change_table :products do |t|
      t.boolean :sold, null: false, default: false
      t.boolean :liked, null: false, default: false
    end
  end
end

# frozen_string_literal: true

class CreateOffers < ActiveRecord::Migration[7.0]
  def change
    create_table :offers do |t|
      t.references :from, null: false, foreign_key: { to_table: :users }
      t.references :to, null: false,   foreign_key: { to_table: :users }
      t.float      :price, default: 0
      t.references :product, null: false, foreign_key: true
      t.string     :status, null: false, default: 'INITIATED'

      t.timestamps
    end
  end
end

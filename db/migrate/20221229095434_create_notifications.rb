# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[7.0]
  def change
    create_table :notifications do |t|
      t.string  :subject,      null: false
      t.string  :body,         null: true
      t.string  :click_action, null: false
      t.boolean :read,         null: false, default: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

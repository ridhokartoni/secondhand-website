# frozen_string_literal: true

class SeedCities < ActiveRecord::Migration[7.0]
  def up
    City.insert_all([
                      { name: 'Solo' },
                      { name: 'Jogja' },
                      { name: 'Jakarta' },
                      { name: 'Bandung' },
                      { name: 'Semarang' }
                    ])
  end

  def down
    City.destroy_all
  end
end

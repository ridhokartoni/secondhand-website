require 'swagger_helper'

RSpec.describe 'Authentication API' do
  path '/users.json' do
    post 'Registration' do
      tags 'Authentication'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            type: :object,
            properties: {
              name: {
                type: :string,
                example: Faker::Games::Dota.hero,
              },
              email: {
                type: :string,
                example: Faker::Internet.email,
              },
              password: {
                type: :string,
                example: Faker::Internet.password,
              },
            },
            required: [ 'name', 'email', 'password' ],
          },
        },
      }

      response '200', 'User registered' do
        let(:user) do
          {
            user: {
              name: Faker::Games::Dota.hero,
              email: Faker::Internet.email,
              password: Faker::Internet.password,
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end

      response '401', 'Email already taken' do
        let(:record) { create(:user) }
        let(:user) do
          {
            user: {
              name: Faker::Games::Dota.hero,
              email: record.email,
              password: Faker::Internet.password,
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                email_already_taken: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end

  path '/users/sign_in.json' do
    post 'Session' do
      tags 'Authentication'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            type: :object,
            properties: {
              email: {
                type: :string,
                example: Faker::Internet.email,
              },
              password: {
                type: :string,
                example: Faker::Internet.password,
              },
            },
            required: [ 'email', 'password' ],
          },
        },
      }

      response '200', 'Session created' do
        let(:password) { Faker::Internet.password }
        let(:record) { create(:user, password:) }
        let(:user) do
          {
            user: {
              email: record.email,
              password: password,
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end

      response '401', 'Email not found' do
        let(:user) do
          {
            user: {
              email: Faker::Internet.email,
              password: Faker::Internet.password,
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                email_not_found: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end

      response '401', 'Wrong Password' do
        let(:record) { create(:user) }
        let(:user) do
          {
            user: {
              email: record.email,
              password: Faker::Internet.password,
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                wrong_password: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end
end

require 'swagger_helper'

RSpec.describe 'Profiles API' do
  path '/profiles.json' do
    put 'Update Profile' do
      tags 'Profiles'
      consumes 'multipart/form-data'
      produces 'application/json'
      security [ bearer_auth: [] ]
      parameter name: :user, in: :formData, schema: {
        type: :object,
        properties: {
          'user[name]': {
            type: :string,
            example: Faker::Games::Dota.hero,
          },
          'user[phone_number]': {
            type: :string,
            example: Faker::PhoneNumber.cell_phone_in_e164,
          },
          'user[address]': {
            type: :string,
            example: Faker::Address.full_address,
          },
          'user[city_id]': {
            type: :number,
            format: :integer,
            example: 1,
          },
          'user[avatar]': {
            type: :string,
            format: :binary,
          },
        },
        required: [ 'user[name]', 'user[address]', 'user[city_id]', 'user[phone_number]' ],
      }

      response '200', 'Profile Updated' do
        let(:Authorization) { bearer_auth(record) }

        let(:record) do
          create(:user)
        end

        let(:user) do
          {
            user: {
              name: Faker::Games::Dota.hero,
              address: Faker::Address.full_address,
              phone_number: Faker::PhoneNumber.cell_phone_in_e164,
              city_id: record.city_id,
              avatar: fixture_file_upload(Rails.root.join('db/assets/avatar.png'), 'image/png'),
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end

    get 'Get Profile' do
      tags 'Profiles'
      produces 'application/json'
      security [ bearer_auth: [] ]

      response '200', 'Profile Retrieved' do
        let(:Authorization) { bearer_auth }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end
end

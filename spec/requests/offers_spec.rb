require 'swagger_helper'

RSpec.describe 'Offers API' do
  path '/offers.json' do
    post 'Create Offer' do
      tags 'Offers'
      consumes 'application/json'
      produces 'application/json'
      security [ bearer_auth: [] ]
      parameter name: :offer, in: :body, schema: {
        type: :object,
        properties: {
          offer: {
            type: :object,
            properties: {
              price: {
                type: :number,
                format: :integer,
                example: 1000,
              },
              to_id: {
                type: :number,
                format: :integer,
                example: 1,
              },
              product_id: {
                type: :number,
                format: :integer,
                example: 1,
              },
            },
            required: [ 'price', 'to_id', 'product_id' ]
          }
        },
      }

      response '201', 'Offer created' do
        let(:Authorization) { bearer_auth }
        let(:merchant) { create(:user) }
        let(:product) { create(:product, user: merchant) }
        let(:offer) do
          {
            offer: {
              price: Faker::Commerce.price(range: 1000..200_000),
              to_id: product.user_id,
              product_id: product.id,
            },
          }
        end

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end

  path '/users/{user_id}/offers.json' do
    get 'List Offers' do
      tags 'Offers'
      produces 'application/json'
      security [ bearer_auth: [] ]
      parameter name: :user_id, in: :path, type: :string

      response '200', 'Offer listed' do
        let(:Authorization) { bearer_auth }
        let(:buyer) { create(:user) }
        let(:products) { create_list(:product, 5, user: user) }
        let!(:offers) do
          products.map do |product|
            create(:offer, from: buyer, to: user, product: product)
          end
        end
        let(:user_id) { buyer.id }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end

  path '/offers/{id}.json' do
    put 'Update Offer' do
      tags 'Offers'
      consumes 'application/json'
      produces 'application/json'
      security [ bearer_auth: [] ]
      parameter name: :id, in: :path, type: :string
      parameter name: :offer, in: :body, schema: {
        type: :object,
        properties: {
          offer: {
            type: :object,
            properties: {
              status: {
                type: :string,
                enum: Offer.statuses.keys,
                example: Offer.statuses.keys.second,
              },
            },
            required: [ 'status' ]
          }
        },
      }

      response '200', 'Offer updated' do
        let(:Authorization) { bearer_auth }
        let(:buyer) { create(:user) }
        let(:product) { create(:product, user: user) }
        let(:record) { create(:offer, from: buyer, to: user, product: product) }
        let(:id) { record.id }
        let(:offer) {
          {
            offer: {
              status: "accepted"
            }
          }
        }

        after do |example|
          content = example.metadata[:response][:content] || {}
          example_spec = {
            "application/json"=>{
              examples: {
                ok: {
                  value: JSON.parse(response.body, symbolize_names: true)
                }
              }
            }
          }

          example.metadata[:response][:content] = content.deep_merge(example_spec)
        end

        run_test!
      end
    end
  end
end

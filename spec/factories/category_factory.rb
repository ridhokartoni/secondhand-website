FactoryBot.define do
  factory(:category) do
    name { Faker::Games::ElderScrolls.creature }
  end
end

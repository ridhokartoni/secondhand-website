FactoryBot.define do
  factory(:offer) do
    price { Faker::Commerce.price(range: 1000..200_000) }
    product { create(:product) }
    from { product.user }
    to { create(:user) }
    status { :initiated }
  end
end

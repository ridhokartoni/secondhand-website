FactoryBot.define do
  factory(:product) do
    name { Faker::Games::Dota.item }
    price { Faker::Commerce.price(range: 1000..200_000) }
    description { Faker::Lorem.paragraph }
    status { :published }
    category { Category.all.sample || create(:category) }
    user { User.all.sample || create(:user) }
    images do
      Dir[Rails.root.join('db/assets/sample*.png')].map do |sample|
        Rack::Test::UploadedFile.new(sample, 'image/png')
      end
    end
  end
end

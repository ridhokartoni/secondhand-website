FactoryBot.define do
  factory(:city) do
    name { Faker::Games::ElderScrolls.city }
  end
end

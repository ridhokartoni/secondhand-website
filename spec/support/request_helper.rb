require 'devise/jwt/test_helpers'
require 'pry'

module RequestHelper
  def response_json
    JSON.parse(response.body)
  end

  def user
    @user ||= create(:user)
  end

  def bearer_auth(actor = user)
    headers = Devise::JWT::TestHelpers.auth_headers({}, actor)
    headers["Authorization"]
  end
end
